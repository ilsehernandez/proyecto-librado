﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Classes
{
    public class ArchivosPedidos
    {
        List<PedidoCompra> compras;
        List<PedidoVenta> ventas;
        List<PedidoVenta> inconclusos;

        public ArchivosPedidos()
        {
            compras = new List<PedidoCompra>();
            ventas = new List<PedidoVenta>();
            inconclusos = new List<PedidoVenta>();
        }

        public List<PedidoCompra> Compras { get => compras; set => compras = value; }
        public List<PedidoVenta> Ventas { get => ventas; set => ventas = value; }
        public List<PedidoVenta> Inconclusos { get => inconclusos; set => inconclusos = value; }
    }
}
