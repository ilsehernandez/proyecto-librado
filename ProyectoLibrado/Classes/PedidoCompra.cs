﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Classes
{
    public class PedidoCompra:Pedido
    {
        public PedidoCompra(DateTime fecha, string mercader, List<Cerveza> cervezas,
            double precio):base(fecha,mercader,cervezas, precio)
        {
        }

        public override void CalcularPrecio()
        {
            foreach (Cerveza cerveza in Cervezas)
            {
                Precio += (cerveza.Cantidad * cerveza.PrecioCompra);
            }
        }
    }
}
