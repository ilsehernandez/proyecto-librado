﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoLibrado.Enums;

namespace ProyectoLibrado.Classes
{
    public class Inventario
    {
        List<Cerveza> lista;

        public Inventario()
        {
            lista = new List<Cerveza>();
        }

        public List<Cerveza> FormulaReabastecimiento()
        {
            List<Cerveza> cervezaPorAgotarse = new List<Cerveza>();
            foreach(Cerveza cerveza in lista)
            {
                if ((int)cerveza.CervezaCarac.Minimo == cerveza.Cantidad)
                    cervezaPorAgotarse.Add(cerveza);
            }
            return cervezaPorAgotarse;
        }

        public void AgregarInventario(PedidoCompra pedido)
        {
            foreach(Cerveza cerveza in pedido.Cervezas)
            {
                foreach(Cerveza cerve in lista)
                {
                    if (cerve.Compare(cerveza.CodigoSerial))
                        cerve.AgregarCantidad(cerveza);
                }
            }
        }

        public void QuitarDeInventario(PedidoVenta pedido)
        {
            
            foreach (Cerveza cerveza in pedido.Cervezas)
            {
                foreach (Cerveza cerve in lista)
                {
                    if (cerve.Compare(cerveza.CodigoSerial))
                        cerve.QuitarCantidad(cerveza);
                }
            }
            
        }

        public void CambiosImorevistos(Cerveza cerveza)
        {
            foreach(Cerveza cerv in lista)
            {
                if (cerv.Compare(cerveza.CodigoSerial))
                {
                    cerv.QuitarCantidad(cerveza);
                }
            }
        }

        public List<Cerveza> BusquedaProductos(List<Cerveza> cervezas)
        {
            List<Cerveza> encontrados = new List<Cerveza>();

            foreach(Cerveza cerveza in lista)
            {
                foreach(Cerveza cerv in cervezas)
                {
                    if (cerv.Compare(cerveza.CodigoSerial))
                        encontrados.Add(cerv);
                }
            }

            return encontrados;
        }

        public List<Cerveza> Lista { get => lista; set => lista = value; }
    }
}
