﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Classes
{
    public class PedidoVenta:Pedido
    {
        bool pedido;

        public PedidoVenta(DateTime fecha, string mercader, List<Cerveza> cervezas,
            bool pedido, double precio):base(fecha,mercader,cervezas, precio)
        {
            this.pedido = pedido;
        }

        public bool Pedido { get => pedido; set => pedido = value; }

        public override void CalcularPrecio()
        {
            foreach(Cerveza cerveza in Cervezas)
            {
                Precio += (cerveza.Cantidad*cerveza.PrecioVenta);
            }
        }

        
    }
}
