﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoLibrado.Enums;

namespace ProyectoLibrado.Classes
{
    public class Cerveza
    {
        string nombre;
        string codigoSerial;
        CervezaCarac cervezaCarac;
        int cantidad;
        double precioCompra;
        double precioVenta;

        public Cerveza(string nombre, string codigoSerial, CervezaCarac cervezaCarac, int cantidad, 
            float precioCompra, float precioVenta)
        {
            this.nombre = nombre;
            this.codigoSerial = codigoSerial;
            this.cervezaCarac = cervezaCarac;
            this.cantidad = cantidad;
            this.precioCompra = precioCompra;
            this.precioVenta = precioVenta;
        }

        public bool Compare(string codigo)
        {
            if (codigoSerial == codigo)
                return true;
            return false;
        }

        public void AgregarCantidad(Cerveza cerv)
        {
            cantidad += cerv.Cantidad;
        }

        public void QuitarCantidad(Cerveza cerv)
        {
            cantidad -= cerv.Cantidad;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string CodigoSerial { get => codigoSerial; set => codigoSerial = value; }
        public CervezaCarac CervezaCarac { get => cervezaCarac; set => cervezaCarac = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double PrecioCompra { get => precioCompra; set => precioCompra = value; }
        public double PrecioVenta { get => precioVenta; set => precioVenta = value; }
    }
}
