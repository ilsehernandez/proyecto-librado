﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Classes
{
    public abstract class Pedido
    {
        DateTime fecha;
        string mercader;
        List<Cerveza> cervezas;
        double precio;

        public Pedido(DateTime fecha, string mercader, List<Cerveza> cervezas, double precio)
        {
            this.fecha = fecha;
            this.mercader = mercader;
            this.cervezas = cervezas;
            this.precio = precio;
        }

        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Mercader { get => mercader; set => mercader = value; }
        public List<Cerveza> Cervezas { get => cervezas; set => cervezas = value; }
        public double Precio { get => precio; set => precio = value; }

        public abstract void CalcularPrecio();
    }
}
