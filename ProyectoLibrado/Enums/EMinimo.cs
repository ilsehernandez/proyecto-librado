﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Enums
{
    public enum Minimo {Min2=2, Min4=4, Min5=5}
    public class EMinimo
    {
        public static string getString(Minimo min)
        {
            switch (min)
            {
                case Minimo.Min2:
                    return "Minimo 2";
                case Minimo.Min4:
                    return "Minimo 4";
                case Minimo.Min5:
                    return "Minimo 5";
                default:
                    return "NA";
            }
        }
    }
}
