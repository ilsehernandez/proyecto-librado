﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Enums
{
    public enum Grado { D0A4, D4A6, D6A8}
    public class EGrado
    {
        public static string getString(Grado grd)
        {
            switch (grd)
            {
                case Grado.D0A4:
                    return "De 0 a 4.0";
                case Grado.D4A6:
                    return "De 4.1 a 6.0";
                case Grado.D6A8:
                    return "De 6.1 a 8.0";
                default:
                    return "NA";
            }
        }
    }
}
