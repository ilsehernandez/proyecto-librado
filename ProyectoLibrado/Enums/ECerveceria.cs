﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Enums
{
    public enum Cerveceria {CerReina, CerPaulander, CerHackerPschorr, CerSchmucker,
        CerBernard, CerMoritz}
    public class ECerveceria
    {
        public static string getString(Cerveceria cerveceria)
        {
            switch (cerveceria)
            {
                case Cerveceria.CerBernard:
                    return "Cerveza Bernard";
                case Cerveceria.CerHackerPschorr:
                    return "Cerveza Hacker -Pshcorr";
                case Cerveceria.CerMoritz:
                    return "Cerveza Moritz";
                case Cerveceria.CerPaulander:
                    return "Cerveza Paulander";
                case Cerveceria.CerReina:
                    return "Cerveza Reina";
                case Cerveceria.CerSchmucker:
                    return "Cerveza Schmucker";
                default:
                    return "NA";
            }
        }
    }
}
