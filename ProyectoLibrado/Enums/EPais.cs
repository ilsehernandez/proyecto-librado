﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoLibrado.Enums
{
    public enum Pais {Mexico, Alemania, RepCheca, Espana}
    public class EPais
    {
        public static string getString(Pais pais)
        {
            switch (pais)
            {
                case Pais.Alemania:
                    return "Alemania";
                case Pais.Espana:
                    return "Espania";
                case Pais.Mexico:
                    return "Mexico";
                case Pais.RepCheca:
                    return "Republica Checa";
                default:
                    return "NA";
            }
        }
    }
}
